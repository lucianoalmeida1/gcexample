﻿using System;
using System.Text;
using System.Windows;


namespace GCExample
{
    /// <summary>
    /// Interaction logic for OtherScreen.xaml
    /// </summary>
    public partial class OtherScreen : Window
    {
        int a = 0;

        StringBuilder b = new StringBuilder(1024 * 1024 * 2);
        public OtherScreen()
        {
            InitializeComponent();
            Singleton.Instance.StringEvent += StringEventHandler;
        }

        public void StringEventHandler(object sender, string arg)
        {
            
        }

        void ClickCollectAll(object sender, RoutedEventArgs e)
        {
            GC.Collect();
        }

        void ClickAllocateLocal(object sender, RoutedEventArgs e)
        {
            for(int i = 0; i < 10000; i++)
            {
                StringBuilder b = new StringBuilder(1024 * 100 /*100Kb*/);
                a = b.Length;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //Singleton.Instance.StringEvent -= StringEventHandler;
        }
    }
}
