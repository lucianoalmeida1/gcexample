﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCExample
{
    public class Singleton
    {
        private static Singleton _inst;
        public static Singleton Instance
        {
            get
            {
                if (_inst == null)
                    _inst = new Singleton();
                return _inst;
            }
            
        }

        private Singleton() { }


        public EventHandler<string> StringEvent; 
    }
}
